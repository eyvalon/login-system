### Description

A login system that uses JWT and BCrypt to securely login unique users and store in LocalStorage. This project uses MongoDB and Vuex.

## Demo

![Alt Text](./images/demo.gif)

## Images

| Sample pictures             | Sample pictures             |
| --------------------------- | --------------------------- |
| ![Alt Text](./images/1.png) | ![Alt Text](./images/2.png) |

**LIST OF IMPLEMENTATIONS**

- [x] Storing JWT token to LocalStorage to let users stay logged in
- [x] Using Vuex state management to handle `login`, `logout` and `registration`
- [x] Using a proper backend in which users need to have a backend that connects to MongoDB at the route `${API_URL}/api/users/authenticate` and `${API_URL}/api/users/register` for logging and registering respectively

**Note**:

You would need to have a Back-End server to replace the **\${API_URL}**. An example template of this can be found [here](https://bitbucket.org/eyvalon/rest-account/src/master/)

---

## Project setup

```
npm install
```

## Running project

To run on default port 8080:

```
npm run serve
```

To run on a certain port (i.e. 3000):

```
npm run serve -- --port 3000
```
